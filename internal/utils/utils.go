package utils

import (
	"fmt"
	"math/rand"
	"time"

	consts "gitlab.com/zacharymeyer/roguelike/internal/constants"
)

func MakeKey(x, y int) string {
	return fmt.Sprintf("%d,%d", x, y)
}

func InBounds(x, y int) bool {
	if x >= 0 && x <= consts.MAP_WIDTH && y >= 0 && y <= consts.MAP_HEIGHT {
		return true
	}
	return false
}

func RandInt(min, max int) int {
	rand.Seed(time.Now().UTC().UnixNano())
	return min + rand.Intn(max-min)
}
