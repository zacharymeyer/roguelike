package game

import (
	"os"

	"code.rocketnine.space/tslocum/gohan"
	"github.com/hajimehoshi/ebiten/v2"

	consts "gitlab.com/zacharymeyer/roguelike/internal/constants"
	"gitlab.com/zacharymeyer/roguelike/internal/system"
	"gitlab.com/zacharymeyer/roguelike/internal/world"
)

type Game struct {
	World *world.GameWorld
}

func NewGame() *Game {
	w := world.NewGameWorld()

	g := &Game{
		World: w,
	}

	g.addSystems()

	return g
}

func (g *Game) Update() error {
	return gohan.Update()
}

func (g *Game) Draw(screen *ebiten.Image) {
	err := gohan.Draw(screen)
	if err != nil {
		panic(err)
	}
}

func (g *Game) Layout(outsideWidth, outsideHeight int) (int, int) {
	return consts.SCREEN_WIDTH, consts.SCREEN_HEIGHT
}

func (g *Game) Exit() {
	os.Exit(0)
}

func (g *Game) addSystems() {
	gohan.AddSystem(system.NewMovementSystem(*g.World.Player, g.World.UnwalkableTiles))

	gohan.AddSystem(system.NewRenderSystem())
}
