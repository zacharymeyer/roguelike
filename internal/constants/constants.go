package constants

const (
	SCREEN_WIDTH  = 640
	SCREEN_HEIGHT = 480
	MAP_WIDTH     = SCREEN_WIDTH
	MAP_HEIGHT    = SCREEN_HEIGHT - 20
	GRID_SIZE     = 10
)
