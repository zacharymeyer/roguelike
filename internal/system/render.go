package system

import (
	"code.rocketnine.space/tslocum/gohan"
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/hajimehoshi/ebiten/v2/text"
	"gitlab.com/zacharymeyer/roguelike/assets"
	"gitlab.com/zacharymeyer/roguelike/internal/component"
	consts "gitlab.com/zacharymeyer/roguelike/internal/constants"
)

type renderSystem struct {
	Position   *component.Position
	Appearance *component.Appearance
}

func NewRenderSystem() *renderSystem {
	return &renderSystem{}
}

func (s *renderSystem) Update(_ gohan.Entity) error {
	return gohan.ErrUnregister
}

func (s *renderSystem) Draw(entity gohan.Entity, screen *ebiten.Image) error {
	x := float64(s.Position.X * consts.GRID_SIZE)
	y := float64(s.Position.Y * consts.GRID_SIZE)

	ebitenutil.DrawRect(screen, x, y, float64(consts.GRID_SIZE), float64(consts.GRID_SIZE), s.Appearance.Bg)

	text.Draw(screen, string(s.Appearance.Char), assets.MindsetNormalFont, int(x+1), int(y+consts.GRID_SIZE), s.Appearance.Fg)
	return nil
}
