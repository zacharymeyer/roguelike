package system

import (
	"code.rocketnine.space/tslocum/gohan"
	"github.com/hajimehoshi/ebiten/v2"
	"gitlab.com/zacharymeyer/roguelike/internal/component"
	"gitlab.com/zacharymeyer/roguelike/internal/grid"
)

type movementSystem struct {
	Position         *component.Position
	MovementPosition *component.Movement

	player gohan.Entity `gohan:"-"`
	walls  []grid.Tile  `gohan:"-"`
}

func NewMovementSystem(player gohan.Entity, walls []grid.Tile) *movementSystem {
	return &movementSystem{player: player, walls: walls}
}

func (s *movementSystem) Update(entity gohan.Entity) error {
	if s.MovementPosition != nil && entity == s.player {
		s.MovementPosition.X, s.MovementPosition.Y = s.moveDirection()
		posX, posY := s.MovementPosition.X+s.Position.X, s.MovementPosition.Y+s.Position.Y

		for _, wall := range s.walls {
			if wall.X == posX && wall.Y == posY {
				return nil
			}
		}

		s.Position.X = posX
		s.Position.Y = posY
	}
	return nil
}

func (s *movementSystem) Draw(_ gohan.Entity, _ *ebiten.Image) error {
	return gohan.ErrUnregister
}

func (s *movementSystem) moveDirection() (x int, y int) {
	if ebiten.IsKeyPressed(ebiten.KeyW) {
		x, y = 0, -1
	} else if ebiten.IsKeyPressed(ebiten.KeyS) {
		x, y = 0, 1
	} else if ebiten.IsKeyPressed(ebiten.KeyA) {
		x, y = -1, 0
	} else if ebiten.IsKeyPressed(ebiten.KeyD) {
		x, y = 1, 0
	}
	return x, y
}
