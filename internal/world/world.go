package world

import (
	"image/color"

	"code.rocketnine.space/tslocum/gohan"
	consts "gitlab.com/zacharymeyer/roguelike/internal/constants"
	"gitlab.com/zacharymeyer/roguelike/internal/entity"
	"gitlab.com/zacharymeyer/roguelike/internal/generation"
	"gitlab.com/zacharymeyer/roguelike/internal/grid"
)

type GameWorld struct {
	Player          *gohan.Entity
	UnwalkableTiles []grid.Tile
}

func NewGameWorld() *GameWorld {
	g := &GameWorld{}

	areaTiles := generation.CreateArea(grid.Rectangle{X1: 0, Y1: 0, X2: consts.MAP_WIDTH / consts.GRID_SIZE, Y2: consts.MAP_HEIGHT / consts.GRID_SIZE}, 1)
	for _, t := range areaTiles {
		switch t.Type {
		case grid.WALL:
			entity.NewWall(t.X, t.Y)
			g.UnwalkableTiles = append(g.UnwalkableTiles, t)
		case grid.FLOOR:
			entity.NewFloor(t.X, t.Y)
		default:
			continue
		}
	}

	p := entity.NewPlayer(consts.MAP_WIDTH/consts.GRID_SIZE/2, consts.MAP_HEIGHT/consts.GRID_SIZE/2, color.RGBA{255, 255, 255, 255}, color.RGBA{0, 0, 0, 255})

	g.Player = p

	return g
}

func LevelCoordsToScreen(x, y int) (int, int) {
	return x * consts.GRID_SIZE, y * consts.GRID_SIZE
}

func ScreenToLevelCoords(x, y int) (int, int) {
	return x / consts.GRID_SIZE, y / consts.GRID_SIZE
}
