package entity

import (
	"image/color"

	"code.rocketnine.space/tslocum/gohan"
	"gitlab.com/zacharymeyer/roguelike/internal/component"
)

func NewWall(x, y int) *gohan.Entity {
	e := gohan.NewEntity()
	e.AddComponent(&component.Wall{})
	e.AddComponent(&component.Appearance{Char: '#', Fg: color.RGBA{0, 0, 255, 255}, Bg: color.RGBA{0, 0, 0, 255}})
	e.AddComponent(&component.Position{X: x, Y: y})
	return &e
}

func NewFloor(x, y int) *gohan.Entity {
	e := gohan.NewEntity()
	e.AddComponent(&component.Floor{})
	e.AddComponent(&component.Appearance{Char: '.', Fg: color.RGBA{128, 128, 128, 255}, Bg: color.RGBA{0, 0, 0, 255}})
	e.AddComponent(&component.Position{X: x, Y: y})
	return &e
}
