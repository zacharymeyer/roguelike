package entity

import (
	"image/color"

	"code.rocketnine.space/tslocum/gohan"
	"gitlab.com/zacharymeyer/roguelike/internal/component"
)

func NewPlayer(dx, dy int, fg, bg color.Color) *gohan.Entity {
	player := gohan.NewEntity()

	player.AddComponent(&component.Position{
		X: dx,
		Y: dy,
	})

	player.AddComponent(&component.Movement{
		X: 0,
		Y: 0,
	})

	player.AddComponent(&component.Appearance{
		Char: '@',
		Fg:   fg,
		Bg:   bg,
	})

	return &player
}
