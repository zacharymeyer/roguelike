package grid

import "math"

type (
	TileType string

	Tile struct {
		X, Y int
		Type TileType
	}

	Rectangle struct {
		X1 int
		Y1 int
		X2 int
		Y2 int
	}
)

const (
	WALL  TileType = "Wall"
	FLOOR TileType = "Floor"
)

func makeRectangle(x1, y1, x2, y2 int, tileType TileType) (tiles []Tile) {
	for i := y1; i <= y2; i++ {
		for j := x1; j <= x2; j++ {
			tiles = append(tiles, Tile{j, i, tileType})
		}
	}
	return tiles
}

func GetRectangle(x, y, width, height int, walls bool, tileType TileType) (*Rectangle, []Tile) {
	var tiles []Tile
	if walls {
		tiles = makeRectangle(x+1, y+1, x+width-2, y+height-2, tileType)
	} else {
		tiles = makeRectangle(x, y, x+width-1, y+height-1, tileType)
	}
	return &Rectangle{x, y, x + width - 1, y + height - 1}, tiles
}

func (r *Rectangle) Center() (int, int) {
	x := int(math.Round(float64(r.X1+r.X2) / 2.0))
	y := int(math.Round(float64(r.Y1+r.Y2) / 2.0))
	return x, y
}

func (r *Rectangle) Intersects(r2 *Rectangle) bool {
	return r.X1 <= r2.X2 &&
		r.X2 >= r2.X1 &&
		r.Y1 <= r2.Y2 &&
		r.Y2 >= r2.Y1
}
