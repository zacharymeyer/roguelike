package generation

import (
	"gitlab.com/zacharymeyer/roguelike/internal/grid"
	"gitlab.com/zacharymeyer/roguelike/internal/utils"
)

const (
	ROOM_MAX_SIZE = 10
	ROOM_MIN_SIZE = 6
	MAX_ROOMS     = 30
)

func CreateArea(r grid.Rectangle, levels int) (lst []grid.Tile) {
	for i := 0; i < levels; i++ {
		levelTiles := createLevel(r.X1, r.Y1, r.X2, r.Y2)
		lst = append(lst, levelTiles...)
	}
	count := 0
	for _, l := range lst {
		if l.Type == grid.FLOOR {
			count++
		}
	}
	return lst
}

func createLevel(x, y, width, height int) (lst []grid.Tile) {
	_, tilesList := grid.GetRectangle(x, y, width, height, false, grid.WALL)
	tiles := make(map[string]grid.Tile)

	for _, tile := range tilesList {
		tiles[utils.MakeKey(tile.X, tile.Y)] = tile
	}

	var rooms []*grid.Rectangle
	for i := 0; i < MAX_ROOMS; i++ {
		tmpWidth := utils.RandInt(ROOM_MIN_SIZE, ROOM_MAX_SIZE)
		tmpHeight := utils.RandInt(ROOM_MIN_SIZE, ROOM_MAX_SIZE)
		tmpX := utils.RandInt(0, width-tmpWidth-1)
		tmpY := utils.RandInt(0, height-tmpHeight-1)

		room, newTiles := createRoom(tmpX, tmpY, tmpWidth, tmpHeight)

		intersects := false
		for _, r := range rooms {
			if r.Intersects(room) {
				intersects = true
				break
			}
		}

		if intersects {
			break
		}

		//if len(rooms) > 0 {
		//  cx, _ := room.Center()
		//  cx2, cy2 := rooms[len(rooms)-1].Center()

		//  for _, tile := range createTunnel(cx, cx2, cy2, true) {
		//    tiles[utils.MakeKey(tile.X, tile.Y)] = tile
		//  }
		//}

		rooms = append(rooms, room)
		merge(tiles, newTiles)
	}

	for _, tile := range tiles {
		lst = append(lst, tile)
	}

	return lst
}

func merge(m1 map[string]grid.Tile, m2 map[string]grid.Tile) {
	for k, v := range m2 {
		m1[k] = v
	}
}

func createRoom(x1, y1, x2, y2 int) (*grid.Rectangle, map[string]grid.Tile) {
	rect, lst := grid.GetRectangle(x1, y1, x2, y2, true, grid.FLOOR)

	tiles := make(map[string]grid.Tile)
	for _, tile := range lst {
		tiles[utils.MakeKey(tile.X, tile.Y)] = tile
	}

	return rect, tiles
}

//func createTunnel(n1, n2, n3 int, horizontal bool) (tiles []grid.Tile) {
//	start := int(math.Min(float64(n1), float64(n2)))
//	end := int(math.Max(float64(n1), float64(n2)) + 1)
//	for i := start; i < end; i++ {
//		tile := grid.Tile{}
//		if horizontal {
//			tile.X = i
//			tile.Y = n3
//		} else {
//			tile.X = n3
//			tile.Y = i
//		}
//		tile.Type = grid.FLOOR
//		tiles = append(tiles, tile)
//	}
//	return tiles
//}
