package component

import "image/color"

type Appearance struct {
	Char rune
	Fg   color.Color
	Bg   color.Color
}
