module gitlab.com/zacharymeyer/roguelike

go 1.18

require (
	github.com/hajimehoshi/ebiten/v2 v2.3.7
	golang.org/x/image v0.0.0-20220601225756-64ec528b34cd
)

require (
	code.rocketnine.space/tslocum/gohan v1.0.0 // indirect
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20220516021902-eb3e265c7661 // indirect
	github.com/gofrs/flock v0.8.1 // indirect
	github.com/jezek/xgb v1.0.1 // indirect
	golang.org/x/exp v0.0.0-20190731235908-ec7cb31e5a56 // indirect
	golang.org/x/mobile v0.0.0-20220518205345-8578da9835fd // indirect
	golang.org/x/sync v0.0.0-20220601150217-0de741cfad7f // indirect
	golang.org/x/sys v0.0.0-20220610221304-9f5ed59c137d // indirect
	golang.org/x/text v0.3.7 // indirect
)
