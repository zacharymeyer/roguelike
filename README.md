# Roguelike

[![pipeline status](https://gitlab.com/zacharymeyer/roguelike/badges/main/pipeline.svg)](https://gitlab.com/zacharymeyer/roguelike/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

![RoguelikeDev Does The Complete Roguelike Tutorial](docs/images/logo.png)

Implementation of [/r/roguelikedev](https://reddit.com/r/roguelikedev/)'s annual [Roguelike Tutorial](https://www.reddit.com/r/roguelikedev/comments/vhfsda/roguelikedev_does_the_complete_roguelike_tutorial/) using [Go](https://go.dev) and [Ebitengine](https://ebiten.org).
