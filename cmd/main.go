package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/hajimehoshi/ebiten/v2"
	"golang.org/x/image/font"
	"golang.org/x/image/font/opentype"

	"gitlab.com/zacharymeyer/roguelike/assets"
	"gitlab.com/zacharymeyer/roguelike/assets/fonts"
	consts "gitlab.com/zacharymeyer/roguelike/internal/constants"
	"gitlab.com/zacharymeyer/roguelike/internal/game"
)

func init() {
	tt, err := opentype.Parse(fonts.Ac437_MindsetTTF)
	if err != nil {
		log.Fatal(err)
	}
	const dpi = 72
	assets.MindsetNormalFont, err = opentype.NewFace(tt, &opentype.FaceOptions{
		Size:    10,
		DPI:     dpi,
		Hinting: font.HintingFull,
	})
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	ebiten.SetWindowTitle("Yet Another Roguelike Tutorial")
	ebiten.SetWindowSize(consts.SCREEN_WIDTH, consts.SCREEN_HEIGHT)
	ebiten.SetWindowClosingHandled(true)

	g := game.NewGame()
	err := ebiten.RunGame(g)
	if err != nil {
		log.Fatal(err)
	}

	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sigc
		g.Exit()
	}()
}
